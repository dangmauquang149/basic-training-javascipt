// let arr=[1,2,3];
// arr[0];

// //truyen value  variable -> object
// let name ='ABC';
// let age=18;
// let object={name,age}

// truyen value object -> variable

let object ={
    name:"ABC",
    age:18,
    birthday:{day:12,month:9,year:2000},
    score:[10,9,8,7],
    fly(){
    //this.name === object.name
    console.log(`$(this.name) can fly `);
    }
};  
// convert object to JSON
let json = JSON.stringify(object);

//convert JSON to object;
// object = JSON.parse(json);

//key ko dc giong nhau
// let {name,age}=object
// object.name="BCD" // thay doi value trong key
object.birthday= ' 12/2/2000'; // them vao object


//arr2 = arr.from();

let object2=Object.assign({},object)
//  console.log('name' in object); // kiem tra name co trong object khoong
//  console.log(object.name !== undefined);//c2
//  console.log(object.hasOwnProperty('name'));//c3
 
 //lay key tu object
//  console.log(Object.keys(object));

//  //lay value tu object
//  console.log(Object.values(object));

 //get keys from object // lay tung property de xu ly
//  Object.keys(object).forEach(key => {
//      console.log(key);
     
//  });

//delete property of object
// delete.values(object)
 
 console.log(JSON.stringify(object));
 
 
