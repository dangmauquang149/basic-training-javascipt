class Animal{
    constructor(name,age,spec){
        this.name = name
        this.age = age
        this.style = spec
    }

    speak(){
        return "meow meow"
    }
    get getAge(){ // lay ra 1 gia tri
        return this.age;
    }
    static sumCatAge(cat1,cat2){ // xu li cac instance voi nhau
        return cat1.age + cat2.age
    }
}

//ke thua
class Dog extends Animal{ //lay gia tri animel o tren
    constructor(name,age,gender,spec="dog"){
        super(name,age,spec);
        this.gender = gender;
    }

    speak(){
        return "whow whow";
    }
}


let cat1 = new Animal ("paw", 2 , "cat");
let cat2 = new Animal ("pew",3,"cat")
let dog1 = new Dog('milu',5,"male")
console.log(cat1);
// check cat1
cat1 instanceof Animal //kiem tra xem cat1 co trong class animal ko
console.log(cat1.speak());
console.log(cat1.getAge);// chỉ lấy ra dc chứ không truyền vào dc
console.log(Animal.sumCatAge(cat1,cat2));



//truy cap vao prototype
Animal.prototype.color = "black";
console.log(cat1.color);
console.log(cat1.hasOwnProperty("color"));
console.log(dog1.hasOwnProperty("name"));

console.log(dog1);
