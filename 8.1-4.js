class Student{
    constructor(ID,firstName,lastName,scores){
        this.ID=ID
        this.firstName = firstName
        this.lastName = lastName
        this.scores = scores
    }
    get averageScore(){
        return (this.scores.math + this.scores.physical + this.scores.chemistry)/3
    }
    static bestStudents(...students){
        let i = 0;
        students.forEach((element,index) => {
            let max = 0
            if(max<element.averageScore){
                max = element.averageScore
                i = index;
            }
        });
        return students[i];
    }
}
let student1 = new Student(1,"Dang","Mau Quang",{math:7.5,physical:6.0,chemistry:7.2})
let student2 = new Student(2,"Huynh","Quy Tuyen",{math:7.0,physical:8.0,chemistry:8.2})
let student3 = new Student(3,"Nguyen","Thanh Nam",{math:8.0,physical:9.0,chemistry:7.0})
console.log(student1.averageScore);
console.log(Student.bestStudents(student1,student2,student3));
let arr = [student1,student2,student3]
let acc = arr.sort((a,b) => a.averageScore < b.averageScore ? -1 : a>b ? 1 : 0);
console.log(acc[0].lastName,acc[1].lastName,acc[2].lastName);
