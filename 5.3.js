let customer = {
    firstName: "John",
    lastName: "Smith",
    dateOfBirth: { day: 6, month: 2, year: 1998 },
    gender: "male",
    phoneNumber: "0775842390",
    email: "john_smith@gmail.com",
    address: "13th Street 47W, New York City"
};
let result = Object.keys(customer).map(key=>{
    if (key === 'dateOfBirh') return {title: key,value :`$(customer[key].day)/$(customer[key].month)/$(customer[key].year)`}
    return {title:key , value:customer[key]};
})
console.log(result);
